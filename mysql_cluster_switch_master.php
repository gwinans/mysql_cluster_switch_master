#!/usr/bin/php
<?php

ini_set('display_errors', 0);

//DB Credentials
$dbuser = 'root'; // Has to be root since you need SUPER privs to view slave status / start / stop slave
$dbpass = ''; // Self-explanitory

$repl_user = 'replication_user'; // Replication user
$repl_user_password = ''; // Self-explanitory

// Config for logging
$do_log = true; // write to switch_master.log, you'll need to create this directory
$verbose = true; // Print to stdout

$mhost = $argv[1];

if( ! filter_var($mhost, FILTER_VALIDATE_IP) ) {
  logit("Invalid IP Address supplied: {$mhost}", $do_log, $verbose);
  exit();
}

logit("Master Host: {$mhost}", $do_log, $verbose);

$masters = [
  //'10.1.1.5', // SQL Nodes w/ binlogging enabled
  //'10.1.1.7'
];

$slaves = [
 // Your list of slaves, ex:
	//'10.1.1.10',
	//'10.1.1.11',
	//'10.1.1.12'
];

logit("Checking if {$mhost} is in the 'masters' array ...", $do_log, $verbose);

if( ! in_array($mhost, $masters) ) {
  logit("{$mhost} not found in the 'masters' array ...", $do_log, $verbose);
?>

The host you specified does not exist in the list of allowed masters.

USAGE: ./switch_master.php [host]

This script will switch a replica to the specified
host if it can obtain the required information... and if
that host is in the list of possible masters.

The currently available hosts to connect a replica to:
<?php
  foreach( $masters as $master ) {
    echo "  {$master}\n";
  }
  exit();
}

$master = new mysqli($mhost, $dbuser, $dbpass, 'mysql');

if( $master->connect_error ) {
  logit("Failed to connect to $mhost", $do_log, $verbose);
  logit(" Error ({$master->connect_errno}) : {$master->connect_error}", $do_log, $verbose);
  exit();
}

logit("Connected to Master Host : {$mhost}", $do_log, $verbose);

if( ! hasBinlogging($master) ) {
  logit("Error: bin_log = off, cannot use this host as a master", $do_log, $verbose);
  logit("Enable bin_log or choose another host", $do_log, $verbose);
  exit();
}

$i = 0;
foreach( $slaves as $shost ) {
  $slave[$i] = new mysqli($shost, $dbuser, $dbpass, 'mysql');
  if( $slave[$i]->connect_error ) {
    logit("Failed to connect to {$shost}\n", $do_log, $verbose);
    logit(" Error ({$slave[$i]->connect_errno}) : {$slave[$i]->connect_error}", $do_log, $verbose);
    exit();
  }
  logit("Connected to Slave Host : {$shost} ... ", $do_log, $verbose);
  $i++;
}

## Begin core logic
foreach( $slave as $shost ) {
  logit("Executing: stopSlave(\$shost)", $do_log, $verbose);
  stopSlave($shost);
  sleep(5); // Sleep a bit to allow the slave process to shut down, it's not always instant
  logit("Checking if the slave process is still running with isSlaveRunning(\$shost)", $do_log, $verbose);
  if( isSlaveRunning($shost) ) { // Check if the slave process is running. If it is, bail out.
    logit("Slave is still running. This script cannot continue. Bailing ...", $do_log, $verbose);
    exit();
  }
  // Slave isn't running, lets get the last Epoch from the slave.
  logit("Retrieving the last epoch value from the slave with getLastEpoch(\$shost)", $do_log, $verbose);
  $epoch = getLastEpoch($shost);
  // We need to sleep a few seconds before we can get the binlog and position from the new master. No idea why.
  sleep(5);
  logit("The last epoch value is: {$epoch}", $do_log, $verbose);
  if( is_int($epoch) ) { // Verify the epoch is an integer
    // Now we retrieve the binlog and position from the *new* master host
    $binlog = getBinlogAndPosition($master, $epoch); // This returns an array

    if( $binlog === false ) {
      logit("getBinlogAndPosition() appears to have failed. It came back false.. complete the change manually.", $do_log, $verbose);
      exit();
    }

    $log = $binlog['binlog'];
    $pos = $binlog['position'];
    logit("binlog = {$log}, pos = {$pos}", $do_log, $verbose);
    logit("Executing: doSlaveChange();", $do_log, $verbose);
    doSlaveChange($mhost, $shost, $repl_user, $repl_user_password, $log, $pos); // This is where the CHANGE MASTER statement is issued to the slave
    logit("Executing: startSlave(\$shost);", $do_log, $verbose);
    startSlave($shost); // Start the slave process
  } else {
    startSlave($shost);
    logit("The returned epoch value is not an integer.. something is wrong! Bailing ...", $do_log, $verbose);
    exit();
  }
}
## End script

## Begin function defs
function logit($message, $do_log = false, $verbose = false) {
  if( $do_log === true ) {
    $date = date("Y-m-d h:i:s");
    $message = "[{$date}] {$message}\n";
    $fp = fopen("switch_master.log", "a");
    fwrite($fp, $message);
  }
  if( $verbose === true ) {
    echo $message;
  }
}

function doSlaveChange($mhost, $host, $repl_user, $repl_user_password, $log, $pos) {
  global $do_log, $verbose;
  $query = "CHANGE MASTER TO MASTER_HOST='{$mhost}', MASTER_USER='{$repl_user}', MASTER_PASSWORD='{$repl_user_password}', MASTER_LOG_FILE='{$log}', MASTER_LOG_POS={$pos}";
  logit("Executing Query: ".str_replace($repl_user_password, "secret", $query), $do_log, $verbose);
  $host->query($query);
}

function stopSlave($host) {
  $host->query("STOP SLAVE");
}

function startSlave($host) {
  $host->query("START SLAVE");
}

function hasBinlogging($host) {
  $res = $host->query("SHOW GLOBAL VARIABLES LIKE 'log_bin'");
  $row = $res->fetch_assoc();
  var_dump($row);
  if( strtolower($row['Value']) === "on" ) {
    return true;
  }
  return false;
}

function isSlaveRunning($host) {
  $res = $host->query("SHOW SLAVE STATUS");
  $row = $res->fetch_assoc();
  if( strtolower($row['Slave_SQL_Running']) === "no" AND strtolower($row['Slave_IO_Running']) === "no" ) {
    return false;
  }
  return true;
}

function getLastEpoch($host) {
  $res = $host->query("select max(epoch) as epoch from mysql.ndb_apply_status");
  $row = $res->fetch_assoc();
  return (int) $row['epoch']; // force int return since it's normally a string value
}

function getBinlogAndPosition($host, $epoch) {
  $res = $host->query("select SUBSTRING_INDEX(File, '/', -1) as binlog, Position as position from mysql.ndb_binlog_index where epoch > {$epoch} order by epoch asc limit 1");
  $row = $res->fetch_assoc();
  return $row;
}